let colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

const renderColorList = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let contentTag = `
      <button class="color-button ${item}"></button> `;
    contentHTML += contentTag;
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderColorList(colorList);

document.addEventListener("DOMContentLoaded", () => {
  const selector = ".color-button";
  const elems = Array.from(document.querySelectorAll(selector));
  const colorClass = document.querySelector("#colorContainer");

  function makeActive(evt) {
    const target = evt.target;

    if (!target || !target.matches(selector)) {
      return;
    }

    elems.forEach((elem) => elem.classList.remove("active"));
    evt.target.classList.add("active");
  }

  colorClass.addEventListener("mousedown", makeActive);
});

document.querySelector(".pallet").addEventListener("click", () => {
  document.getElementById("house").className = "house pallet";
});
document.querySelector(".viridian").addEventListener("click", () => {
  document.getElementById("house").className = "house viridian";
});
document.querySelector(".pewter").addEventListener("click", () => {
  document.getElementById("house").className = "house pewter";
});
document.querySelector(".cerulean").addEventListener("click", () => {
  document.getElementById("house").className = "house cerulean";
});
document.querySelector(".vermillion").addEventListener("click", () => {
  document.getElementById("house").className = "house vermillion";
});
document.querySelector(".lavender").addEventListener("click", () => {
  document.getElementById("house").className = "house lavender";
});
document.querySelector(".celadon").addEventListener("click", () => {
  document.getElementById("house").className = "house celadon";
});
document.querySelector(".saffron").addEventListener("click", () => {
  document.getElementById("house").className = "house saffron";
});
document.querySelector(".fuschia").addEventListener("click", () => {
  document.getElementById("house").className = "house fuschia";
});
document.querySelector(".cinnabar").addEventListener("click", () => {
  document.getElementById("house").className = "house cinnabar";
});
