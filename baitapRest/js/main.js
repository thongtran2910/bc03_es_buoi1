let sum = (array) => {
  let total = 0;
  for (let i = 0; i < array.length; i++) {
    total += array[i];
  }
  return total;
};
function tinhDTB(...input) {
  let arraySum = sum(input);
  return arraySum / input.length;
}

document.getElementById("btnKhoi1").addEventListener("click", () => {
  diemToan = document.getElementById("inpToan").value * 1;
  diemLy = document.getElementById("inpLy").value * 1;
  diemHoa = document.getElementById("inpHoa").value * 1;
  document.getElementById("tbKhoi1").innerHTML = tinhDTB(
    diemToan,
    diemLy,
    diemHoa
  );
});
document.getElementById("btnKhoi2").addEventListener("click", () => {
  diemVan = document.getElementById("inpVan").value * 1;
  diemSu = document.getElementById("inpSu").value * 1;
  diemDia = document.getElementById("inpDia").value * 1;
  diemEng = document.getElementById("inpEnglish").value * 1;
  document.getElementById("tbKhoi2").innerHTML = tinhDTB(
    diemVan,
    diemSu,
    diemDia,
    diemEng
  );
});
